﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Got
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<Character> Characters { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            Characters = LoadCharacters();
        }

        public void LoadCharactersListBox(string subString)
        {
            CharactersListBox.Items.Clear();
            foreach (var character in Characters)
            {
                if (character.Name.ToLower().IndexOf(subString) != -1)
                {
                    CharactersListBox.Items.Add(new ListBoxItem { Content = character.Name });
                }
            }
        }

        public List<Character> LoadCharacters()
        {
            using (WebClient client = new WebClient())
            {
                string json = client.DownloadString("https://api.got.show/api/book/characters");
                return JsonConvert.DeserializeObject<List<Character>>(json);
            }
        }

        private void SearchButtonClick(object sender, RoutedEventArgs e)
        {
            LoadCharactersListBox(NameTextBox.Text.ToLower());
        }

        private void CharactersListBoxSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CharactersListBox.SelectedItems.Count > 0)
            {
                foreach (var character in Characters)
                {
                    if (character.Name == (CharactersListBox.SelectedItem as ListBoxItem).Content.ToString())
                    {
                        MessageBox.Show(character.House);
                    }
                }
            }
        }

        private void NameTextBoxKeyDown(object sender, KeyEventArgs e)
        {
            string key = e.Key.ToString();

            if (key == "Return")
            {
                LoadCharactersListBox(NameTextBox.Text.ToLower());
            }
        }
    }
}